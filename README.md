# building an alpinelinux docker/singularity image for CI

Tru <tru@pasteur.fr>

(toy) docker/singularity containers

Docker:
```
docker run -ti registry-gitlab.pasteur.fr/tru/docker-singularity-alpine-ci:main
```
Singularity:
```
singularity run oras://registry-gitlab.pasteur.fr/tru/docker-singularity-alpine-ci:latest
```

Dockefile is used to build the initial container, then singularity is used
to convert it to a OCI image, ready to be used.

## why ?
- leverage in a single project the 2 initials projects https://gitlab.pasteur.fr/tru/docker-alpine-ci and https://gitlab.pasteur.fr/tru/singularity-alpine-ci
- working with gitlab CI as for github CI https://github.com/truatpasteurdotfr/singularity-docker-busybox/
