FROM alpine:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

RUN apk update && apk upgrade
#RUN apk add --nocache wget lftp curl vim
RUN date +"%Y-%m-%d-%H%M" > /last_update
